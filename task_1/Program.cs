﻿using System;
using System.Linq;

class Program
{
    static void Main()
    {
        int a = 65539;
        int c = 0;
        ulong m = 4294967296;
        int n = 200;
        int K = 30000;

        uint seed = 1;
        uint[] randomSequence = LinearCongruentialGenerator((ulong)a, (ulong)c, m, seed, K);
        int[] boundedSequence = randomSequence.Select(x => (int)(x % (uint)n)).ToArray();

        int[] intervalCounts = new int[n];
        foreach (int value in boundedSequence)
        {
            intervalCounts[value]++;
        }

        double[] probabilities = intervalCounts.Select(count => (double)count / K).ToArray();

        double meanValue = boundedSequence.Average();

        double variance = boundedSequence.Select(x => Math.Pow(x - meanValue, 2)).Average();

        double stdDeviation = Math.Sqrt(variance);

        Console.WriteLine("Частота інтервалів появи випадкових чисел:");
        Console.WriteLine(string.Join(", ", intervalCounts));

        Console.WriteLine("\nСтатистична ймовірність появи випадкових чисел:");
        Console.WriteLine(string.Join(", ", probabilities.Select(p => p.ToString("F4"))));

        Console.WriteLine($"\nМатематичне сподівання випадкових чисел: {meanValue:F4}");
        Console.WriteLine($"Дисперсія випадкових чисел: {variance:F4}");
        Console.WriteLine($"Середньоквадратичне відхилення випадкових чисел: {stdDeviation:F4}");
    }

    static uint[] LinearCongruentialGenerator(ulong a, ulong c, ulong m, uint seed, int size)
    {
        uint[] sequence = new uint[size];
        ulong x = seed;
        for (int i = 0; i < size; i++)
        {
            x = (a * x + c) % m;
            sequence[i] = (uint)x;
        }
        return sequence;
    }
}
